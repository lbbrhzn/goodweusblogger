#!/bin/bash

sudo systemctl is-active --quiet goodweusblogger.service
if [ $? -eq 0 ]
then
    sudo systemctl stop goodweusblogger.service
fi
